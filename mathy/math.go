package mathy

// Double multiplies the provided number by 2
func Double(i int) int {
	return i * 2
}


// CollatzGen creates a simple generator to iterate through a Collaz Conjecture sequence
// That is, given a term, if it is odd, the next term is 3n + 1, otherwise it's n/2. 
// See:  https://en.wikipedia.org/wiki/Collatz_conjecture
func CollatzGen(start uint64) func() uint64 {
	current := start

	return func() uint64 {
		if current == 1 {
			return 1
		}
		
		if current %2 == 0 {
			current = current/2
		} else {
			current = (current * 3) + 1
		}
		return current
	}
}