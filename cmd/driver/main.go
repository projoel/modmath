package main

import (
	"fmt"
	"gitlab.com/projoel/modmath/mathy"
)

func main() {
	fmt.Println("Yo")

	doubleNum := 100
	fmt.Printf("If you double %v, you get: %v", doubleNum, mathy.Double(doubleNum))

	var lastNum uint64 = 27
	collatz := mathy.CollatzGen(lastNum);

	fmt.Printf("Collatz sequence for i = %v\n> %v", lastNum, lastNum)
	for lastNum != 1 {
		lastNum = collatz()
		fmt.Printf(" -> %v", lastNum)
	}
	fmt.Println("\nThanks for checking in!")
}
